-- @module model
local model = {}
--
local etlua = require 'etlua'
local lfs = require 'lfs' -- luafilesystem
local lume = require 'satelito.lib.lume.lume'
local dirtree = require 'satelito.dirtree'
local file = require 'satelito.file'
local markdown = require 'discount' -- lua-discount
local template = require 'satelito.template'
local inspect = require 'inspect'

function model.set(filepath)
  local pagedata = file.get_metafile(filepath) or {}
  local config = _G.Satelito.config
  local contentdir = _G.Satelito.contentdir
  local publicdir = _G.Satelito.publicdir
  local templates = _G.Satelito.templates
  local time_created

  -- If required properties are nil
  pagedata.title = pagedata.title or file.get_basename(filepath):match('(.+)%..*')
  pagedata.date = pagedata.date or os.date('%Y-%m-%d', lfs.attributes(filepath).change)
  pagedata.datetime = pagedata.datetime or os.date('%H:%M:%S', lfs.attributes(filepath).change)

  -- Path properties
  pagedata.contentdir = contentdir
  pagedata.path = filepath
  pagedata.relpath = file.get_relpath(filepath, contentdir)
  pagedata.reldir = pagedata.relpath:match("(.*/)")

  -- Link properties
  pagedata.rellink = file.get_rellink(filepath, contentdir)
  pagedata.rellinkdir = '/'.. (pagedata.reldir ~= nil and pagedata.reldir or '')
  pagedata.permalink = file.get_permalink(filepath, contentdir, config.siteurl)
  pagedata.exportlink = file.get_exportlink(filepath, contentdir, publicdir)
  pagedata.dirlink = file.get_permalink(filepath, contentdir, config.siteurl):match("(.*/)")

  -- Time properties
  time_created = (pagedata.date..pagedata.datetime):gsub('%W','')

  pagedata.time_created = time_created
  pagedata.time_modification = lfs.attributes(filepath).modification
  pagedata.time_modified_child = file.get_lastmodified(lume.array(dirtree.get(file.get_dirname(filepath))))

  -- Unique page ID for the Atom feed
  pagedata.id = 'tag:' .. config.siteurl:match('^%w+://([^/]+)') .. ',' .. pagedata.date .. ':' .. pagedata.rellink
  pagedata.idorder = pagedata.rellinkdir .. time_created

  -- HTML content
  if file.is_content(filepath) then
    local that_content

    -- Lua compilation (with etlua) is needed in content
    if pagedata.lua then
      local lua_in_content = etlua.compile(file.read(filepath))

      that_content = lua_in_content(lume.extend({}, config, pagedata))
    else
      that_content = file.read(filepath)
    end

    if file.is_markdown(filepath) then
      pagedata.content = markdown(that_content)
    elseif file.is_html(filepath) then
      pagedata.content = that_content
    end
  end

  -- Summary
  if  pagedata.summary then
    pagedata.summary = markdown(pagedata.summary)
  end

  -- List (and Feed)
  if file.is_index(filepath) and pagedata.list ~= false then
    pagedata.list = file.get_list(filepath, contentdir)
  end

  -- Collection
  -- File list
  if pagedata.collection then
    pagedata.collection_list = file.get_collection(pagedata.collection)
  end

  -- Change the language for a specific content
  pagedata.language = pagedata.language or config.language

  -- Templates
  pagedata.template = pagedata.template or pagedata.posttype or 'default'
  pagedata.layout = pagedata.layout or 'layout'
  pagedata.head = pagedata.head or 'head'
  pagedata.navigation = pagedata.navigation or 'navigation'
  pagedata.footer = pagedata.footer or 'footer'
  pagedata.feed = pagedata.feed or 'feed.xml'

  -- Include a partial template and compile it
  -- @usage <%- include("test-partial") %>
  pagedata.include = function(templatename)
    local inc = etlua.compile(file.read(template.find(templates, templatename)))

    return inc(lume.extend({}, config, pagedata))
  end

  return lume.extend({}, config, pagedata, {templates = templates})
end

return model
