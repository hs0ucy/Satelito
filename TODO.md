* The `init` command should test if git is installed.
* Make sure `list = false` and `feed = false` works.
* Add an CLI option to change the base URL on build.
* Tags should be lowecase to bypass doublons.
* Add more feedback for exec control
* ~~Add the --verbose option for the pipe command.~~
* ~~Set a GPL 3 licence to Satelito.~~
* ~~Add the possibility to put lua in markdown/html files.~~
