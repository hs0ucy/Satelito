-- @module assets
local assets = {}
--
local file = require 'satelito.file'
local lfs = require 'lfs' -- luafilesystem
local lume = require 'satelito.lib.lume.lume'
local mimetypes = require 'mimetypes'

function assets.export(filemeta)
  local mtypes = filemeta.mimetypes
  local siblings = lume.array(lfs.dir(file.get_dirname(filemeta.paths.content..filemeta.relpath)))
  local function is_in_mimetypes(f)
    return lume.any(mtypes, function(mtype) return mtype == mimetypes.guess(f) end)
  end

  for i = 1, #siblings do
    if is_in_mimetypes(siblings[i]) then
      local source = file.get_dirname(filemeta.paths.content..filemeta.relpath)..siblings[i]
      local target = file.get_dirname(filemeta.exportlink)..siblings[i]

      if not lfs.attributes(target)
        or lfs.attributes(source).modification > lfs.attributes(target).modification
      then
        print('=> Moving assets ...')
        print('====> '..source..' => '..target)

        os.execute('cp ' .. source ..  ' ' .. target)
      end
    end
  end

  return
end

return assets
