-- @module sitemapxml
local sitemapxml = {}
--
local etlua = require 'etlua'
local file = require 'satelito.file'
local template = require 'satelito.template'

function sitemapxml.make(sitedata, templates, destination)
  local _sitemapxml = etlua.compile(file.read(template.find(templates, 'sitemap.xml')))
  local sitemapxml_xml = _sitemapxml({sitemap = sitedata})
  local sitemapxml_xml_path = destination..'sitemap.xml'

  return sitemapxml_xml, sitemapxml_xml_path
end

return sitemapxml
