-- @module feed
local feed = {}
--
local etlua = require 'etlua'
local lume = require 'satelito.lib.lume.lume'
local file = require 'satelito.file'
local template = require 'satelito.template'

function feed.make(filemeta)
  local _feed = etlua.compile(file.read(template.find(filemeta.templates, filemeta.feed)))
  local feed_xml = _feed(lume.extend({}, filemeta))
  local feed_xml_path = filemeta.exportlink:match('(.+)%..*')..'.xml'

  return feed_xml, feed_xml_path
end

return feed
