-- @module site
local site = {}
--
local inspect = require 'inspect'
local lfs = require 'lfs'
--
local assets = require 'satelito.assets'
local feed = require 'satelito.feed'
local file = require 'satelito.file'
local list = require 'satelito.list'
local lume = require 'satelito.lib.lume.lume'
local page = require 'satelito.page'
local sitemapxml = require 'satelito.sitemapxml'

--- From a filepath get the closest 'config.lua' by climbing the
-- directory tree
-- Recursive function
function site.get_config(filepath)
  assert(filepath and filepath ~= '', 'The filepath parameter is missing or empty.')

  local dir = filepath:match("(.*/)")
  local dir_parent = string.sub(dir, 1, -2):match("(.*/)")

  for entry in lfs.dir(dir) do
    if entry and entry == 'config.lua' then
      return dir .. entry
    end
  end

  return site.get_config(dir_parent)
end

-- Make the site (from the sitedata)
function site.make(sitedata)
  local duration
  local config = _G.Satelito.config
  local contentdir = _G.Satelito.contentdir
  local export = _G.Satelito.args['export']
  local verbose = _G.Satelito.args['verbose']
  local make = _G.Satelito.args['make']
  local publicdir = _G.Satelito.publicdir
  local templates = _G.Satelito.templates
  local timestart = _G.Satelito.timestart

  for i = 1, #sitedata do
    local html, html_path
    local feed_xml, feed_xml_path
    local paginated

    if verbose then
      io.write('   ¬ ./' .. config.paths.content..sitedata[i].relpath)
    end

    sitedata[i].index = i

    -- Lists
    -- Subpages from the actual index page
    if sitedata[i].list ~= false and file.is_index(sitedata[i].path) then
      sitedata[i].children = list.get_children(sitedata[i].list, sitedata, sitedata[i].asc)
    end

    -- Collections
    -- Group of pages from specific relpaths
    if sitedata[i].collection and sitedata[i].collection_list then
      sitedata[i].collection = list.get_children(sitedata[i].collection_list, sitedata, sitedata[i].asc)
    end

    -- Archives
    if sitedata[i].archives then
      sitedata[i].archives_children = list.get_archives(contentdir)
    end

    -- Tags
    if sitedata[i].tags then
      sitedata[i].tags_children = list.get_tags(contentdir)
    end

    if i > 1 then
      sitedata[i].relprev = sitedata[i-1]
    end

    if i < #sitedata then
      sitedata[i].relnext = sitedata[i+1]
    end

    -- If a pagination is requested
    if sitedata[i].pagination and file.is_index(sitedata[i].relpath) then
      print('=> A pagination is requested ...')

      sitedata[i].pagination.limit = sitedata[i].pagination.limit or 6
      sitedata[i].pagination.prefix = sitedata[i].pagination.prefix or 'pg-'

      -- Split page children by pagination limit
      paginated = list.set_pagination(sitedata[i].collection or sitedata[i].children, sitedata[i].pagination.limit)

      -- Set the pagination length
      sitedata[i].pagination.length = #paginated

      print('=> Making the pagination pages ...')

      for p = 1, #paginated do
        sitedata[i].children = paginated[p]
        sitedata[i].pagination.current = p
        -- Make the pagination pages
        if p == 1 then
          html, html_path = page.make(sitedata[i])
        else
          html, html_path = page.make(
            sitedata[i],
            sitedata[i].exportlink:match("(.*/)")..sitedata[i].pagination.prefix..p..'.html'
          )
        end
        -- Export the pagination pages
        if export then
          file.export(html_path, html)
        else
          print(html)
        end
      end

    else
      -- Make the page
      html, html_path = page.make(sitedata[i])

      -- Export the page
      if export then
        file.export(html_path, html)
      else
        print(html)
      end
    end

    -- Feed
    if file.is_index(sitedata[i].relpath)
      and export
      and sitedata[i].list ~= false
    then
      feed_xml, feed_xml_path = feed.make(sitedata[i])
      file.export(feed_xml_path, feed_xml)
    end

    -- Copy assets to the public_html/ folder
    if export then
      assets.export(sitedata[i])
    end

    if verbose then
      io.write(' > ./'.. config.paths.public_html:sub(1, -2) .. sitedata[i].rellink .. ' ✔ \n')
    end
  end

  -- Make and export the sitemap.xml
  if config.sitemapxml and make and export then
    local sitemapxml_xml, sitemapxml_xml_path = sitemapxml.make(
      sitedata, templates, publicdir
    )

    file.export(sitemapxml_xml_path, sitemapxml_xml)
  end

  duration = os.difftime(os.time(), timestart) > 0
    and 'in approximately '..os.difftime(os.time(), timestart)..' second(s).'
    or 'in less than 1 second.'

  print('--------------------------------------------------------------------------')
  print('Satelito built '..lume.count(sitedata)..' HTML page(s) '..duration)
end

return site
