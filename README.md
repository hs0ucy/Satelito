# Satelito

Satelito is a static site generator (SSG) made with Lua.

## Introduction

Satelito uses Markdown (and HTML) for basic content and optionally Lua
files for metadata. There is no front matter support; but Lua tables
are used for storing functions that can extend the functionality of
your site.

For templates, Satelito uses the [Etlua templating
language](https://github.com/leafo/etlua). It is simple but powerful,
because it allows you to run Lua scripts directly in templates.

Through the use of Satelito you become familiar with [Lua, a fast
lightweight programming language](https://www.lua.org/about.html)
that has a [small learning curve](https://learnxinyminutes.com/docs/lua/).

See the project [wiki](https://codeberg.org/hs0ucy/Satelito/wiki) for
further details.

## Features

* Ability to create a static website from Markdown (or HTML) files.
* Can generate pages individually or by directory.
* Automatically creates lists of subpages, for all index pages.
* Define [page
  collection](https://codeberg.org/hs0ucy/Satelito/wiki/Collection)
  from directory or page name.
* Automatically creates Atom/RSS syndication files, for all index
  pages.
* Fast; a site with hundreds of pages can be generated in seconds.
* Hackable thanks to Lua which can be used directly in
  templates or configuration files.
* Can automatically create a sitemap.xml file.
* Create
  [pagination](https://codeberg.org/hs0ucy/Satelito/wiki/Pagination)
  with sequential navigation.
* And more...

## Installation

To install Satelito, you must have [Luarocks](https://luarocks.org/)
and [Git](https://git-scm.com/) on your computer:
<https://github.com/luarocks/luarocks/wiki/Download#installing>.

Then in your terminal, run:

    luarocks install satelito --local

If the installation succeeded, you can test Satelito by invoking the help page:

    satelito -h

## Init a website

To create a new project, use the `init` sub-command:

`$ satelito init`

Then Satelito will clone the [satelito
sample](https://codeberg.org/hs0ucy/Satelito-sample) website into your
home directory.

You should rename `~/satelito-sample` and edit `~/sample/config.lua`
according to your needs.

Then in the folder's project where is the `config.lua` file, type:

    satelito make --export

And that's it, your new site is now built in the `public_html/` folder.

---

See Satelito in action in this short video:

[![asciicast](https://asciinema.org/a/598180.svg)](https://asciinema.org/a/598180)

## Basic concepts and conventions

### The configuration file

The `config.lua` file is the file Satelito goes to
generate your website. You can edit it, add metadata and use them in
your templates.

The `paths` sub-table tells Satelito where the content, templates and
the public_html folders are:

    paths = {
        content = 'content/',
        templates = 'templates/',
        public_html = 'public_html/',
    }

The `mimetypes` sub-table determines what kind of *non-textual
content* will be exported in your website tree:

    mimetypes = {
        'image/svg+xml',
        'image/gif',
        'image/jpeg',
        'image/png',
        'application/pdf',
    }

See an example of [a configuration file
here](https://codeberg.org/hs0ucy/Satelito-sample/src/branch/master/config.lua).

### Also a page generator

Satelito is designed more like a **static page generator** than a
*static site generator* (SSG), because with the sub-command `pipe`,
you can input Markdown and HTML files through the pipeline
to create one page or a small group of pages, instead of rebuilding
everything each time.

Here's an example that builds only one file with the output of the `echo`
command:

    $ echo ~/satelito-sample/content/filmography/the-eclipse.md | satelito pipe

Or all of the files in the `filmography/` directory:

    $ find ~/satelito-sample/content/filmography/ | satelito pipe

This choice brings versatility to Satelito. You can render only a part
of your website without needing to regenerate the whole project. And
as the last two examples show, this allows you to use it with other
programs in the Unix toolbox!

### The rule of two

Like I said above, with Satelito there is no possibility of *front
matter* in Markdown files. If you want metadata for your content, you
have to create a Lua file with the same name as the Markdown
file.

    -rw-r--r-- 1 hs hs 115 Fec 29 15:16 trip-to-the-moon.lua
    -rw-r--r-- 1 hs hs 801 Fec 29 15:17 trip-to-the-moon.md

#### The metadata file anatomy

A metadata file, in its simplest expression, should look like this:

    return {
      date = "2020-09-01",
      datetime = "14:49:00",
      title = "A Trip to the Moon",
    }

`date`, `datetime` and `title`, are the only required fields.

Note that Satelito will not return an error if a Markdown file does
not have a Lua equivalent: an HTML file will be created with the
name of the file, followed by the current date and time.

## Usage

The easiest way to build your website is with the `make` sub-command. Go
to the main folder with your `config.lua` inside, and then run:

    satelito make --export.

The `--export` option is to write the files in the `public_html/`
folder, otherwise it will only display all the HTML files in the
terminal (the same behaviour applies for the `pipe` sub-command).

Search recursively for all Markdown (or HTML) files with `find`, and pipe
directly as input to `satelito` with the `pipe` sub-command:

    find ~/satelito-sample/content | satelito pipe

Same thing but with [`ag`](https://geoff.greer.fm/ag/):

    ag '' --markdown --html -l ~/satelito-sample/content | satelito pipe

Search for all the Markdown and HTML files of the first level with `find`:

    find ~/satelito-sample/content -maxdepth 1 | satelito pipe.

Watch change with [`entr`](http://eradman.com/entrproject/) to rebuild the project:

    find ~/satelito-sample/ | entr -s 'satelito make --export && echo "Rebuilt at $(date +%T)"'
    
[![asciicast](https://asciinema.org/a/598662.svg)](https://asciinema.org/a/598662)

## Theme

There is no theme mechanism because it's not meant to get in your way with that.
