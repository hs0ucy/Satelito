-- @module page
local page = {}
--
local etlua = require 'etlua'
local lume = require 'satelito.lib.lume.lume'
local file = require 'satelito.file'
local template = require 'satelito.template'

--- Build a page from markdown/lua to HTML
-- @name page.make
-- @param filepath a pathname to a markdown file
-- @return a string that is an HTML code block
-- @return a string that is a filepath

function page.make(filemeta, exportpath)
  -- Compile different parts of the page
  local head = etlua.compile(file.read(template.find(filemeta.templates, filemeta.head)))
  local navigation = etlua.compile(file.read(template.find(filemeta.templates, filemeta.navigation)))
  local post = etlua.compile(file.read(template.find(filemeta.templates, filemeta.template)))
  local footer = etlua.compile(file.read(template.find(filemeta.templates, filemeta.footer)))
  local layout = etlua.compile(file.read(template.find(filemeta.templates, filemeta.layout)))

  -- Then put them all together
  local html = layout(
    lume.extend({},
      filemeta,
      {post = post(lume.extend({}, filemeta))},
      {head = head(lume.extend({}, filemeta))},
      {navigation = navigation(lume.extend({}, filemeta))},
      {footer = footer(lume.extend({}, filemeta))}
    )
  )
  -- Get the target location of the page
  local html_path = exportpath ~= nil and exportpath or filemeta.exportlink

  return html, html_path
end

return page
