-- @module file
local file = {}
--
local lfs = require 'lfs'
local mimetypes = require 'mimetypes'
local lume = require 'satelito.lib.lume.lume'
local dirtree = require 'satelito.dirtree'

-- Check if a file exists
function file.exists(filepath)
  local _file = io.open(filepath, 'r')

  if _file ~= nil then
    _file:close()

    return filepath
  else
    return false
  end
end

-- Open & read the content of a file
function file.read(filepath)
  if not filepath then
    print('A file is missing ...')
  end

  local _file = assert(io.open(filepath, 'r'))
	local content = _file:read '*a'

	_file:close()

	return content
end

-- Create a file and add data in it
function file.write(filepath, data)
  local _file = io.open(filepath, 'w+')

  _file:write(data)
  _file:close()

  return assert(lfs.attributes(filepath).mode == 'file')
end

-- Get basename from a file path
function file.get_basename(filepath)
  return filepath:gsub('(.*/)(.*)', '%2')
end

-- Get the directory of a file
function file.get_dirname(filepath)
  return filepath:match("(.*/)") and filepath:match("(.*/)") or ''
end

-- Get directory basename
function file.get_basedir(dirname)
  return dirname:match('^.+/(.+)$')
end

function file.get_relpath(filepath, dirname)
  return filepath:sub((dirname):len() + 1)
end

function file.get_permalink(filepath, dirname, url)
  return url .. '/' .. file.get_relpath(filepath, dirname):match('(.+)%..*') .. '.html'
end

function file.get_exportlink(filepath, dirname, htmlpath)
  return htmlpath .. file.get_relpath(filepath, dirname):match('(.+)%..*') .. '.html'
end

function file.get_metafile(filepath)
  if lfs.attributes(filepath:match('(.+)%..*') .. '.lua') then
    return dofile(filepath:match('(.+)%..*') .. '.lua')
  else
    return nil
  end
end

-- Get the modified file from a list of paths
function file.get_lastmodified(filelist)
  return math.max(
    table.unpack(
      lume.map(filelist,
               function(fl)
                 return lfs.attributes(fl).modification
               end
  )))
end

-- Check if the file is markdown
function file.is_markdown(filepath)
  return (mimetypes.guess(filepath) == 'text/x-markdown')
end

-- Check if the file is HTML
function file.is_html(filepath)
  return (mimetypes.guess(filepath) == 'text/html')
end

-- Check if the file is index.md
function file.is_index(filepath)
  return (file.get_basename(filepath) == 'index.md') or (file.get_basename(filepath) == 'index.html')
end

-- Check if the file is content (markdown or html)
function file.is_content(filepath)
  assert(filepath, filepath..' does not exists')
  return (mimetypes.guess(filepath) == 'text/x-markdown') or (mimetypes.guess(filepath) == 'text/html')
end

-- Check if the file is lua
function file.is_lua(filepath)
  return (mimetypes.guess(filepath) == 'text/x-lua')
end

function file.get_rellink(filepath, dirname)
  if  file.is_index(filepath) then
    return '/' .. file.get_dirname(file.get_relpath(filepath, dirname))
  else
    return '/' .. file.get_relpath(filepath, dirname):match('(.+)%..*') .. '.html'
  end
end

-- Get a list of relative children paths from a filepath
function file.get_list(filepath, dirname)
  local collection = {}

  for subfilepath in dirtree.get(file.get_dirname(filepath)) do
    if subfilepath
      and file.is_content(subfilepath)
      and not file.is_index(subfilepath)
    then
      collection[#collection+1] = file.get_relpath(subfilepath, dirname)
    end
  end

  return collection
end

-- Get a table collection of contents
function file.get_collection(collection)
  if collection and type(collection) == 'table' then
    local collection_list = {}
    local contentdir = _G.Satelito.contentdir

    for i = 1, #collection do
      if lfs.attributes(contentdir..collection[i]).mode == 'directory' then
        collection_list[#collection_list+1] = file.get_list(contentdir..collection[i], contentdir)
      else
        collection_list[#collection_list+1] = { collection[i] }
      end
    end

    return lume.concat(table.unpack(collection_list))
  end

  return
end

-- Create a dirtree
function file.mkdir(filepath)
  local sep, pStr = package.config:sub(1, 1), ''
  local _filepath = lfs.currentdir()..file.get_relpath(filepath, lfs.currentdir())

  for dir in _filepath:gmatch('[^' .. sep .. ']+') do
    pStr = pStr .. sep .. dir
    lfs.mkdir(pStr)
  end
end
--

--- Export a file to a specific location
-- @name file.export
-- @param filepath the location path of the file
-- @param filecontent a string that is a code block
-- @return execute a file.write function
function file.export(filepath, filecontent)
  file.mkdir(file.get_dirname(filepath))
  return file.write(filepath, filecontent)
end

return file
