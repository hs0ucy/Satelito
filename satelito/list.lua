-- @module list
local list = {}
--
local dirtree = require 'satelito.dirtree'
local file = require 'satelito.file'
local model = require 'satelito.model'
local lume = require 'satelito.lib.lume.lume'

-- Pagination
function list.set_pagination(pagelist, len)
  local slicedlist = {}
  local i = 1

  while(i < #pagelist) do
    local j

    if i == 1 then
      j = 1
      i = i + (len-1)
    else
      j = i + 1
      i = i + len
    end

    slicedlist[#slicedlist+1] = lume.slice(pagelist, j, i)
  end

  return slicedlist
end

-- Get children contents
function list.get_children(children_list, sitedata, asc)
  local children = {}
  local relpaths = {}
  local contentdir = _G.Satelito.contentdir

  if children_list then
    for i = 1, #sitedata do
      relpaths[#relpaths+1] = sitedata[i].relpath
    end

    for i = 1, #children_list do
      local is_exists = lume.find(relpaths, children_list[i])

      if is_exists then
        children[#children+1] = sitedata[is_exists]
      else
        children[#children+1] = model.set(contentdir..children_list[i])
      end
    end

    -- Sorting ASC if list have asc set to true
    if asc then
      table.sort(children, function(a, b) return tonumber(a.time_created) < tonumber(b.time_created) end)
    else
      table.sort(children, function(a, b) return tonumber(a.time_created) > tonumber(b.time_created) end)
    end
  end

  return children
end

-- Archives
function list.get_archives(contentdir)
  local archives_table = {}

  for filepath in dirtree.get(contentdir) do
    if file.is_markdown(filepath)
      or file.is_html(filepath)
      and file.get_metafile(filepath)
    then
      local metafile = file.get_metafile(filepath)

      if metafile.date ~= '0000-00-00' then
        local year = string.sub(metafile.date, 1, 4)
        local month = string.sub(metafile.date, 6, 7)

        if archives_table[year] then
          archives_table[year][month] = {}
        else
          archives_table[year] = {
            [month] = {}
          }
        end
      end
    end
  end

  for filepath in dirtree.get(contentdir) do
    if file.is_markdown(filepath)
      or file.is_html(filepath)
      and file.get_metafile(filepath)
    then
      local metafile = file.get_metafile(filepath)

      if metafile.date ~= '0000-00-00' then
        local year = string.sub(metafile.date, 1, 4)
        local month = string.sub(metafile.date, 6, 7)
        local day = string.sub(metafile.date, 9, 10)
        local archive_info = {
          day = day,
          rellink = file.get_rellink(filepath, contentdir),
          title = metafile.title
        }

        archives_table[year][month][#archives_table[year][month]+1] = archive_info
      end
    end
  end

  return archives_table
end

-- Tags
function list.get_tags(contentdir)
  local tags_table = {}

  -- Insert all the keywords as key of an empty subtable
  for filepath in dirtree.get(contentdir) do
    if file.is_markdown(filepath)
      or file.is_html(filepath)
      and file.get_metafile(filepath)
    then
      local metafile = file.get_metafile(filepath)

      if metafile.keywords ~= nil then
        for _, keyword in pairs(file.get_metafile(filepath).keywords) do
          if keyword ~= '' then
            tags_table[keyword] = {}
          end
        end
      end
    end
  end

  -- After insert relative links in each keyword's table
  for filepath in dirtree.get(contentdir) do
    if file.is_markdown(filepath)
      or file.is_html(filepath)
      and file.get_metafile(filepath)
    then
      local metafile = file.get_metafile(filepath)

      if metafile.keywords ~= nil then
        for _, keyword in pairs(file.get_metafile(filepath).keywords) do
          if tags_table[keyword] then
            local tags_info = {
              title = metafile.title,
              rellink = file.get_rellink(filepath, contentdir)
            }

            tags_table[keyword][#tags_table[keyword]+1] = tags_info
          end
        end
      end
    end
  end

  return tags_table
end

return list
