-- @module dirtree
local dirtree = {}
--
local lfs = require 'lfs' -- luafilesystem
local inspect = require 'inspect'

function dirtree.get(dir)
  assert(dir and dir ~= '', 'directory parameter is missing or empty')

  -- Removes slash if is one
  if string.sub(dir, -1) == '/' then
    dir = string.sub(dir, 1, -2)
  end

  -- Main function of the coroutine (recursive)
  local function yieldtree(dir)
    for entry in lfs.dir(dir) do
      if entry ~= '.' and entry ~= '..' then
        entry = dir..'/'..entry

        local attr = lfs.attributes(entry)

        coroutine.yield(entry,dir,attr)

        if attr.mode == 'directory' then
          yieldtree(entry)
        end
      end
    end
  end

  return coroutine.wrap(function() yieldtree(dir) end)
end

-- Reset the public_html/ directory
-- Recursive function
function dirtree.reset(dir)
  local mimetypes = require 'mimetypes'
  local publicdir = _G.Satelito.publicdir
  local _dir = dir

  -- Removes slash if is one
  if string.sub(dir, -1) == '/' then
    _dir = string.sub(_dir, 1, -2)
  end

  for entry in lfs.dir(_dir) do
    local type
    if entry ~= '.'
      and entry ~= '..'
      and entry ~= '.gitignore'
    then
      entry = _dir..'/'..entry
      type = lfs.attributes(entry).mode

      if type == 'file'
        and (mimetypes.guess(entry) == 'text/html' or mimetypes.guess(entry) == 'text/xml')
      then
        os.remove(entry)
        print('❌ '.. entry)
      elseif type == 'directory' then
        dirtree.reset(entry)
      end
    end
  end

  if dir ~= publicdir then
    lfs.rmdir(dir)
  end
end

return dirtree
