-- @module template
local template = {}
--
local lume = require 'satelito.lib.lume.lume'

-- Find the path to a template file from his name
function template.find(templates, templatename)
  local _template = lume.match(
    templates,
    function(tpl)
      -- Try to find a match
      return string.gsub(tpl, '(.*/)(.*)', '%2') == templatename .. '.html'
    end
  )

  return _template
end

return template
