#!/usr/bin/env lua
--
local argparse = require 'argparse'
local inspect = require 'inspect'
local lfs = require 'lfs'
--
local lume = require 'satelito.lib.lume.lume'
local model = require 'satelito.model'
local dirtree = require 'satelito.dirtree'
local file = require 'satelito.file'
local site = require 'satelito.site'
--
local init
local pipe
local make
local exec
local reset

local parser = argparse()
  :name 'satelito'
  :description 'Satelito is a static site generator in lua script.'
  :epilog 'For more info, see https://codeberg.org/hs0ucy/Satelito'
local args

-- Set 'init' command
init = parser:command('init', 'Init the sample website in your $HOME folder.')

-- Set 'pipe' command
pipe = parser:command('pipe', 'Build the site by inputing one or more markdown files through the pipeline.')
pipe:flag('-e --export', 'Export the outputed HTML in the *config.paths.public_html* folder.')
pipe:flag('-v --verbose', 'Displays in the terminal the source and destination of each content.')

-- Set 'make' command
make = parser:command('make', 'Build the site from his directory.')
make:flag('-e --export', 'Export the outputed HTML in the *config.paths.public_html* folder.')
make:flag('-v --verbose', 'Displays in the terminal the source and destination of each content.')

-- Set the exec command
exec = parser:command('exec', 'Execute a script from the site\'s bin directory.')
exec:argument 'bin name'
-- Set the reset command
reset = parser:command('reset', 'Delete HTML and XML files in the public_html directory.')

args = parser:parse()

-- Started
print('=> ☾ Satelito is here ☽ ...')

-------------
-- GLOBALS --
-------------

-- Set the Satelito global table
_G.Satelito = {}
_G.Satelito.timestart = os.time()
_G.Satelito.args = args

if not args['init'] then
  -- Put config.lua in a table
  print('=> Fetching the configuration file content ...')
  _G.Satelito.config = dofile(site.get_config(lfs.currentdir()..'/'))

  -- Change current directory for the config.lua's directory
  print('=> Moving where the site configuration file is located ...')
  lfs.chdir(file.get_dirname(site.get_config(lfs.currentdir()..'/')))

  -- Then add the current directory to the package.path
  package.path = package.path .. ';'.. lfs.currentdir() ..'/?.lua'

  -- Get the absolute path for the 'content' directory
  _G.Satelito.contentdir = lfs.currentdir()..'/'.._G.Satelito.config.paths.content

  -- Get the absolute path for the 'public_html' directory
  _G.Satelito.publicdir = lfs.currentdir()..'/'.._G.Satelito.config.paths.public_html

  -- Get the list of templates
  if args['make'] or args['pipe'] then
    print('=> Fetching the templates ...')
    _G.Satelito.templates = lume.array(dirtree.get(lfs.currentdir() .. '/' .. _G.Satelito.config.paths.templates))
  end
end

--print(inspect(_G.Satelito))

----------
-- EXEC --
----------

if args['exec'] then
  package.path = package.path .. ';'.. lfs.currentdir() ..'/?.lua'

  print(args['bin name'], inspect(lfs.currentdir()))

  if args['bin name'] then
    dofile(lfs.currentdir() .. '/bin/'..args['bin name'])
  end

  return
end

----------
-- INIT --
----------

-- Initialize the satelito sample site in $HOME
-- Example '$ satelito init'
if args['init'] then
  os.execute('mkdir ~/satelito-sample')
  os.execute('git clone https://codeberg.org/hs0ucy/Satelito-sample.git ~/satelito-sample')
  print('------------------------------------------------------------------------------------------------------')
  os.execute('rm -Rf ~/satelito-sample/.git')
  os.execute('ls -la ~/satelito-sample')
  print('------------------------------------------------------------------------------------------------------')
  print('You should rename `~/satelito-sample` and edit `~/satelito-sample/config.lua` according to your needs.')
  print('------------------------------------------------------------------------------------------------------')

  return
end

----------
-- PIPE --
----------

-- Pipe stdout into satelito
-- Example: '$ find site/content/ | satelito pipe'
if args['pipe'] then
  local sitedata = {}

  for filepath in (io.lines()) do
    if file.is_content(filepath) then
      -- Get the pagedata of the file
      local pagedata = model.set(filepath)

      -- Add the pagedata of the file into the sitedata table
      if lume.count(sitedata) == 0 then
        print('=> Fetching the markdown and HTML content ...')
      end

      sitedata[#sitedata+1] = pagedata
    end
  end

  print('=> '..lume.count(sitedata)..' content found')
  print('=> Making the web site ...')

  -- Sorting by alphanum
  table.sort(lume.unique(sitedata), function(a, b) return a.idorder > b.idorder end)

  return site.make(sitedata)
end

----------
-- MAKE --
----------

-- Make command
-- Example: '$ satelito make --export'
if args['make'] then
  local contentdir = _G.Satelito.contentdir
  local sitedata = {}

  print('=> Fetching the markdown and HTML content ...')

  for filepath in dirtree.get(contentdir) do
    if file.is_content(filepath) then
      local pagedata = model.set(filepath)

      sitedata[#sitedata+1] = pagedata
    end
  end

  print('=> '..lume.count(sitedata)..' content found')
  print('=> Making the web site ...')

  -- Sort before make the website
  table.sort(lume.unique(sitedata), function(a, b) return a.idorder > b.idorder end)

  return site.make(sitedata)
end

-----------
-- RESET --
-----------

if args['reset'] then
  local publicdir = _G.Satelito.publicdir
  local yes_or_no

  repeat
    print('=> Reset ...')
    print('=> Are you sure you want to delete HTML and XML files in the "'..publicdir..'" folder?')
    print('=> Please answer with "Yes" or "No" ...')
    io.flush()

    yes_or_no = string.lower(tostring(io.read()))
  until yes_or_no == 'yes' or yes_or_no == 'no'

  if yes_or_no == 'yes' then
    dirtree.reset(publicdir)

    print('=> Done')
  else
    print('=> Aborted')
  end

  return
end
